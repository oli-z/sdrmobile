#!/bin/bash
echo "Adding network adapter tun0..."
sudo ip tuntap add dev apn0 mode tun user $USER group $USER
echo "Assigning IP to network adapter..."
sudo ip addr add 192.168.42.0/24 dev apn0
echo "Enabling network adapter..."
sudo ip link set apn0 up
echo "Enabling IP forwarding..."
sudo sysctl -w net.ipv4.ip_forward=1
echo "Setting iptables rules for forwarding and NAT..."
sudo iptables -A FORWARD -o wlp7s0 -i apn0 -s 192.168.42.0/24 -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
sudo iptables -t nat -A POSTROUTING -o wlp7s0 -j MASQUERADE
